#include <SPI.h>
#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>
#include <MQTT.h>
#include <ArduinoJson.h>

//Configuración WiFi
char ssid[] = "";
char password[] = "";

WiFiClient net;
MQTTClient client;

unsigned long lastMillis = 0;

void connect() {
  Serial.print("checking wifi...");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(1000);
  }
  
  Serial.print("\nconnecting...");
  while (!client.connect("meteo","<usuario>","<clave>")) {
  //while (!client.connect("ARDUINO_NO_GENUINO")) {
    Serial.print(".");
    delay(1000);
  }

  Serial.println("\nconnected!");

  //client.subscribe("/test");
  // client.unsubscribe("/hello");
}

void messageReceived(String &topic, String &payload) {
  Serial.println("incoming: " + topic + " - " + payload);

}

void setup() {
  Serial.begin(115200);

    // kss._debug = true;

    // Set WiFi to station mode and disconnect from an AP if it was Previously
    // connected
    WiFi.mode(WIFI_STA);
    WiFi.disconnect();
    delay(100);

    // Attempt to connect to Wifi network:
    Serial.print("Connecting Wifi: ");
    Serial.println(ssid);
    WiFi.begin(ssid, password);
    while (WiFi.status() != WL_CONNECTED)
    {
        Serial.print(".");
        delay(500);
    }
    Serial.println("");
    Serial.println("WiFi connected");
  client.begin("84.125.225.80",6974, net);
  client.onMessage(messageReceived);

  connect();
}

void loop() {
  client.loop();
  if (!client.connected()) {
    connect();
  }

  StaticJsonDocument<1024> doc;
  doc["message"] = "Testing json";
  doc["time"]=millis();

  String json="";
  serializeJson(doc, json);
  // publish a message roughly every second.
  if (millis() - lastMillis >= 5000) {
    lastMillis = millis();
    Serial.println("Publicando mensaje...");
    client.publish("arduino/meteo/datatest", json);
  }
}
