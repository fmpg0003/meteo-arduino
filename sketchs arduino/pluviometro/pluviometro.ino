/*
El pluviometro tiene una pequeña cubeta con una capacidad de aproximadamente 0.28 mm de agua.
Una vez se llena con esta cantidad, la cubeta se vacía y manda una señal.
Dicha señal la capturamos añadiendo un interruptor al pin donde se conecta el pluviometro (en este caso el 3)
Multiplicando por 0.28 calculamos cuantos milimetros de agua han caido en el tiempo dado.
Cada Milimetro de agua equivaldría a 1 litro por metro cuadrado, habría que calcular los mm 
*/


#define pinAgua 3

int contadorLluvia=0;


void setup() {
  // put your setup code here, to run once:
Serial.begin(9600);
attachInterrupt(digitalPinToInterrupt(pinAgua),medicionLluvia, CHANGE);
}

void loop() {
  // Ejemplo de como deberia funcionar
  delay(10000); //dejamos un delay de 10 segundos para que pueda medir
  Serial.print("mililitros || L/M2: ");
  Serial.println(contadorLluvia*.28); //mostramos cuantos milimetros hay (cada contador*0.28mm por contador)

    
}

//Metodo que se llama en la interrupción
void medicionLluvia(){
   contadorLluvia++;
}
