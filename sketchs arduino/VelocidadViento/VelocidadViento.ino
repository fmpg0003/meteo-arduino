/*
Sketch para medir la velocidad del viento con un anemometro y un arduino
Se utilizan interrupciones, las cuales, cuando se da un evento (en este caso rotación de las copas del anemometro) llaman a una función.
Puesto que estamos usando un Arduino Uno, los únicos pines que tienen la posibilidad de ejecutar interrupciones son el 2 y el 3

En el anemometro que estamos usando (WS3000) y similares, un pulso en un segundo equivale a una velocidad del viento de 2.4 KM/H 
Por lo que la velocidad vendrá dada por la cuenta V=(P/T)*2.4 Donde V es la velocidad P son las pulsaciones y T es el tiempo de medición
*/

#include <math.h>
#define PinVelocidadViento (2) // Pin del anemometro
const int TiempoMedicion = 3; //Tiempo durante el cual se va a tomar la medición
volatile unsigned long Contador; // Contador usado para las interrupciones (nº de veces que se activa el sensor)
float VelocidadViento; // Velocidad del viento

void setup() {
  Serial.begin(9600); 
}

void loop() {

  
  medicionVelocidadViento();
  Serial.print("Velocidad del viento: ");
  Serial.print(VelocidadViento);
  Serial.print(" kh/h - ");
  Serial.print(VelocidadViento/3.6);
  Serial.println(" m/s");
}

/*Función que se llama para realizar la medición de la velocidad del viento
 *Añade una interrupción al pin del anemometro de tal manera que llama a una función cuando el estado del pin pasa de 0 a 1
 */
void medicionVelocidadViento(){
    Contador = 0; //Contador a 0
    Serial.println("Iniciando Medición de Velocidad del viento");
    attachInterrupt(digitalPinToInterrupt(PinVelocidadViento),rotacionAnemometro,RISING); //Añadimos una Interrupción al pin del anemometro
                                                                                          //Llamamos a la función cuando el estado pasa de 0 a 1 (RISING)
    delay(1000*TiempoMedicion);//Realizamos un delay de los segundos indicados
    detachInterrupt(digitalPinToInterrupt(PinVelocidadViento)); //Desactivamos las interrupciones para que deje de contar
    VelocidadViento = ((float)Contador / (float)TiempoMedicion) *2.4; // V = (P/T) * 2.4
}

// Función que se llama en la interrupción para calcular la velocidad del viento
void rotacionAnemometro () {
  Contador++;
}
