
int direccion;
int valorVeleta;
String cardinal;
void setup(){
  Serial.begin(9600);
}

void loop(){
  
 medicionDireccionViento();
  delay(2000);
}


void medicionDireccionViento(){
  valorVeleta=analogRead(A4);
  direccion=map(valorVeleta,0,1024,0,5000);
  if(direccion==3833 || direccion==3837){
    cardinal="Este";
  } else if(direccion==1977 || direccion==1982 || direccion==1987){
    cardinal="Este-SurEste"; 
  } else if(direccion==2250 || direccion==2255 || direccion==2260){
    cardinal="SurEste";
  } else if(direccion==410 || direccion==400 || direccion==405){
    cardinal="Sur-SurEste";
  } else if(direccion==322 || direccion==317 || direccion==312){
    cardinal="Sur-SurOeste";
  }else if(direccion==449 || direccion==444 || direccion==454){
    cardinal="Sur"; 
  } else if (direccion==615 || direccion==610){
    cardinal="Oeste-SurOeste";
  } else if(direccion==893 || direccion==903 || direccion==922){
    cardinal="SurOeste";
  }else if(direccion==1396 || direccion==1401){
    cardinal="Oeste";
  } else if(direccion==1191 ||direccion==1186){
    cardinal="Oeste-NorOeste";
  } else if(direccion==3076 || direccion==3081){
    cardinal="NorOeste";
  } else if(direccion==2924){
    cardinal="Norte-NorOeste";
  } else if(direccion==4614 || direccion==4619){
    cardinal="Norte";
  } else if(direccion==4042){
    cardinal="Norte-Noreste";
  } else if(direccion==4331 ||direccion==4335){
    cardinal="NorEste";
  } else if(direccion==3427 || direccion==4332 || direccion==4322){
    cardinal="Este-NorEste";
  }
  Serial.print("Dirección del viento: ");
  Serial.print(cardinal);
  Serial.print(" - Valor: ");
  Serial.println(direccion);
}
