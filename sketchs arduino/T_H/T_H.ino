#include "DHT.h" //Libreria DTH humedad y temperatura

//Defines para los pines y tipo de sensores
#define LDRPIN A1
#define DHTPIN 2 //Pin de lectura
#define DHTTYPE DHT11 //Tipo de sensor DHT

//Creacion de los objetos
DHT dht(DHTPIN,DHTTYPE); //Declaramos el objeto del sensor

//Setup
void setup() {
  //Inicialización del serial
  Serial.println("Inicializando...");
  Serial.begin(9600);
  while(!Serial){
    Serial.print(".");
  }
  Serial.println("Serial inicializado, inicializando componentes...");
  //Inicialización de componentes
  dht.begin(); //inicializamos el sensor DHT
  pinMode(LDRPIN, INPUT); //Activamos el pin para la lectura de luminidad del LDR
  
}

//Loop 
void loop() {
  delay(1000); //Lectura a cada segundo
  lectura(); //Lectura de datos
    
 }


//Metodo de lectura de datos
 void lectura(){
    float humedad = dht.readHumidity(); //Lee la humedad
    float temperatura = dht.readTemperature(); //Lee la temperatura en Celsius
    //float temperaturaFarenheit = dht.readTemperature(true); //Si se le pasa un true como parametro, lee la temperatura en Farenheit
    int input=analogRead(LDRPIN); //Lectura de luminosidad
    
    if(isnan(humedad) || isnan(temperatura)){ //Si no funciona el sensor DHT
      Serial.println("Fallo en la lectura del sensor"); //tira fallo
    } else{ //Prints de todo
      Serial.print("Humedad: ");
      Serial.print(humedad);
      Serial.print("%");
      Serial.print("  |  "); 
      Serial.print("Temperature: ");
      Serial.print(temperatura);
      Serial.print("°C ~ ");
      Serial.print("  |  "); 
      Serial.print("Luminosidad: ");
      Serial.print(input);
      }
      Serial.println("");
 }
