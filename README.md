# Estación meteorológica con Arduino

Este es un proyecto para la asignatura de Inteligencia Ambiental en la Universidad de Jaén. Para más información sobre el proyecto, dirigíos a [la página wiki del proyecto](https://dv.ujaen.es/ilias.php?ref_id=1220659&page=Creaci%C3%B3n_de_una_estaci%C3%B3n_meteorol%C3%B3gica_con_la_que_medir_diversos_parametros&cmd=preview&cmdClass=ilwikipagegui&cmdNode=11q:ny:11t&baseClass=ilwikihandlergui).
