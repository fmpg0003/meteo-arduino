/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ej4;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.StringJoiner;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

/**
 *
 * @author DMGILA
 */
public class ArduinoDataReceiver implements MqttCallback{
    public static void main(String[] args) throws IOException {
        ArduinoDataReceiver program=new ArduinoDataReceiver();
        program.execute();
    }
    
    String topic_sub    = "arduino/meteo/data";
    int qos             = 2;
    String broker       = "tcp://84.125.225.80:6974";
    String clientId     = "JavaClientMeteo";
    String request_url  = "http://84.125.225.80:6972/request.php";
    MemoryPersistence persistence = new MemoryPersistence();
    MqttClient sampleClient;
    
    //La clase Auth es una clase auxiliar que contiene variables estáticas con los datos de inicio de sesión
    //Por razones obvias de seguridad, no vamos a mostrar el contenido de dicha clase.
    String usuario=Auth.MQTT_USERNAME;
    String clave=Auth.MQTT_PASSWORD;

    public ArduinoDataReceiver() {
        try {
            this.sampleClient = new MqttClient(broker, clientId, persistence);
        } catch (MqttException ex) {
            ex.printStackTrace();
        }
    }
    
    public void execute(){
        try {
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setUserName(usuario);
            connOpts.setPassword(clave.toCharArray());
            connOpts.setCleanSession(true);
            System.out.println("Conectándose al broker: " + broker);
            sampleClient.connect(connOpts);
            sampleClient.setCallback(this);
            sampleClient.subscribe(topic_sub);
            System.out.println("Conectado y escuchando en el topic: "+topic_sub);
            System.out.println("------------------------------------------------\n");
        } catch(MqttException me) {
            System.out.println("reason "+me.getReasonCode());
            System.out.println("msg "+me.getMessage());
            System.out.println("loc "+me.getLocalizedMessage());
            System.out.println("cause "+me.getCause());
            System.out.println("excep "+me);
            me.printStackTrace();
        }
    }

    @Override
    public void connectionLost(Throwable thrwbl) {
        
    }

    @Override
    public void messageArrived(String topic, MqttMessage msg) {
        try{
            
            //Parsear los datos, sacar cada componente y almacenarlo en una variable local
            String mensaje=msg.toString();
            JsonObject rootObj = JsonParser.parseString(mensaje).getAsJsonObject();
            String dir_viento=rootObj.get("dv").getAsString();
            String precipitaciones=rootObj.get("mm").getAsString();
            String vel_viento=rootObj.get("vv").getAsString();
            String humedad=rootObj.get("h").getAsString();
            String temp=rootObj.get("t").getAsString();
            String luz=rootObj.get("lu").getAsString();
            
            //Mostrar información por pantalla
            System.out.println("--RECIBIDOS DATOS DESDE MQTT: "+mensaje+"--");
            if(vel_viento.equals("0"))
                System.out.println("Viento: 0Km/h");
            else
                System.out.println("Viento: "+dir_viento+" a "+vel_viento+"Km/h");
            if(temp.equals("FLS"))
                System.out.println("Temperatura: --");
            else
                System.out.println("Temperatura: "+temp+"ºC");
            if(humedad.equals("FLS"))
                System.out.println("Humedad: --");
            else
                System.out.println("Humedad: "+humedad+"%");
            System.out.println("Cantidad de luz: "+luz+" unidades");
            System.out.println("Precipitaciones: "+precipitaciones+" mm");
            System.out.println();
            System.out.println("Enviando los datos al servidor web...");
            
            sendRequest(temp, humedad, dir_viento, vel_viento, luz, precipitaciones);
            
            System.out.println("-----\n\n");
        } catch(Exception ex){
            ex.printStackTrace();
        }
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken imdt) {
    }
    
    
    public void sendRequest(String... params) throws MalformedURLException, IOException{
        //Solo se miden los primeros 6 parámetros. Si no hay 6, la request no funcionará
        if(params.length<6){
            throw new IllegalArgumentException("Incorrecto número de parámetros");
        }
        
        //Inicio de la conexión
        URL url = new URL(request_url);
        URLConnection con = url.openConnection();
        HttpURLConnection http = (HttpURLConnection)con;
        http.setRequestMethod("POST");
        http.setDoOutput(true);
        
        //Parámetros de la query
        Map<String,String> arguments = new HashMap<>();
        arguments.put("user", Auth.REQUEST_USERNAME);
        arguments.put("pwd", Auth.REQUEST_PASSWORD);
        arguments.put("t", params[0]);
        arguments.put("h", params[1]);
        arguments.put("dv", params[2]);
        arguments.put("vv", params[3]);
        arguments.put("lu", params[4]);
        arguments.put("mm", params[5]);
        
        //Mandar petición correctamente formateada
        StringJoiner sj = new StringJoiner("&");
        for(Map.Entry<String,String> entry : arguments.entrySet())
            sj.add(URLEncoder.encode(entry.getKey(), "UTF-8") + "=" 
                 + URLEncoder.encode(entry.getValue(), "UTF-8"));
        byte[] out = sj.toString().getBytes(StandardCharsets.UTF_8);
        int length = out.length;
        http.setFixedLengthStreamingMode(length);
        http.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        http.connect();
        try(OutputStream os = http.getOutputStream()) {
            os.write(out);
        }
        int status=http.getResponseCode();
        
        //Mirar código de estado que manda la web. Este no es el mensaje final, sino si la conexión se ha completado correctamente o no
        switch(status){
            case 200: //OK
            case 201: //Created
                System.out.print("Conexión realizada correctamente. Código de error recibido: ");
                
                //Coger los datos que ha enviado el servidor (Mensaje JSON)
                String response_json;
                try(BufferedReader br = new BufferedReader(new InputStreamReader(http.getInputStream()))){
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line).append("\n");
                    }
                    response_json=sb.toString();
                }
                JsonObject json = JsonParser.parseString(response_json).getAsJsonObject();
                
                //Coger código de error del servidor (Este sí es el código de error de la aplicación)
                int error_code=json.get("error_code").getAsInt();
                System.out.println(error_code);
                switch(error_code){
                    case 0: //OK
                        System.out.println("Consulta realizada correctamente. Datos almacenados en el servidor.");
                        return;
                    case 1: //Petición vacía
                        System.out.println("Error mandando la petición al servidor.");
                        return;
                    case 2: //Petición incompleta (faltan campos)
                        System.out.println("La petición es incorrecta.");
                        return;
                    case 3: //MariaDB inaccesible
                        System.out.println("Hay un error en la conexión a la base de datos.");
                        return;
                    case 4: //Unauthorized
                        System.out.println("Error de usuario y/o contraseña.");
                        return;
                    default: //No debería ocurrir, implica un error en la descomposición del json o un mensaje nuevo que no es tratado
                        System.out.println("Desconocido.");
                }
                break;
            default:
                System.out.println("Error conectando al servidor web. No se ha enviado ningún dato (Código de error "+status+").");
        }
        
        System.out.println();
        
    }
}
