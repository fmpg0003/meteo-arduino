CREATE TABLE `meteo`.`data` (
  `timestamp` BIGINT(8) NOT NULL,
  `temperatura` FLOAT NOT NULL,
  `humedad` INT NOT NULL,
  `dir_viento` ENUM('N', 'S', 'E', 'O', 'NE', 'NO', 'SE', 'SO') NULL,
  `vel_viento` FLOAT NOT NULL,
  `luz` INT NOT NULL,
  `precipitaciones` FLOAT NOT NULL,
  PRIMARY KEY (`timestamp`));

CREATE TABLE `meteo`.`auth` (
  `user` VARCHAR(45) NOT NULL,
  `pwd` VARCHAR(200) NOT NULL,
  PRIMARY KEY (`user`));