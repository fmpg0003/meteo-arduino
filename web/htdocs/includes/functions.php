<?php

function getDataFromDay($timestamp){
    $timestamp_dia = $timestamp + (3600*24);
    $mysqli=new mysqli("localhost", "meteo", "HayQueAprobarAmbientales", "meteo");
    if($mysqli->connect_error){
        return -1;
    }
    $stmt=$mysqli->prepare("SELECT * FROM data WHERE timestamp >= '$timestamp' AND timestamp <= '$timestamp_dia'");
    $stmt->execute();
    $res=$stmt->get_result();

    if($res->num_rows == 0){
        return 0;
    }
    $resultado=array("datos"=>array(), "error_code"=>0);
    while($row=$res->fetch_assoc()){
        $fecha_hora=date("d-m-y H:i", $row['timestamp']);
        $resultado["datos"][$fecha_hora]=array("timestamp"=>$row['timestamp'],"temperatura" => $row['temperatura'], "humedad" => $row['humedad'], "dir_viento" => $row['dir_viento'], "vel_viento" => $row['vel_viento'], "luz" => $row['luz'], "precipitaciones" => $row['precipitaciones']);
    }
    return json_encode($resultado, JSON_PRETTY_PRINT);
}