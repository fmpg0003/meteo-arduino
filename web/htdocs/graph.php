<?php // content="text/plain; charset=utf-8"
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

if( !isset($_GET['data']) ||
    !isset($_GET['labels']) ||
    !isset($_GET['title']) )
{
    die;
}
$datos=explode(",",$_GET['data']);
$labels=explode(",", $_GET['labels']);
$title=$_GET['title'];
require_once ('includes/jpgraph/src/jpgraph.php');
require_once ('includes/jpgraph/src/jpgraph_line.php');

$datay1 = $datos;

// Setup the graph
$graph = new Graph(800,350);
$graph->SetScale("textlin");

$theme_class=new UniversalTheme;

$graph->SetTheme($theme_class);
$graph->img->SetAntiAliasing(false);
$graph->title->Set($title);
$graph->SetBox(false);

$graph->SetMargin(40,20,36,63);

$graph->img->SetAntiAliasing();

$graph->yaxis->HideZeroLabel();
$graph->yaxis->HideLine(false);
$graph->yaxis->HideTicks(false,false);

$graph->xgrid->Show();
$graph->xgrid->SetLineStyle("solid");

$graph->xaxis->SetTickLabels($labels);
$graph->xgrid->SetColor('#E3E3E3');

// Create the first line
$p1 = new LinePlot($datay1);
$graph->Add($p1);
$p1->SetColor("#6495ED");

$graph->legend->SetFrameWeight(1);

// Output line
$graph->Stroke();

?>

