<?php // content="text/plain; charset=utf-8"
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require("includes/functions.php");
if(!isset($_POST['fecha'])){
    header("Location: index.php");
    die;
}

$res=getDataFromDay(strtotime($_POST['fecha']));

if($res === 0 || $res === 1){
    header("Location: index.php");
    die;
}
$json=json_decode($res, true);
$date=date("d/m/y", strtotime($_POST['fecha'])+(3600*12));

//Estas 4 variables permiten calcular la temperatura y humedad media para el día completo
$media_t=0;
$temps=0;
$media_h=0;
$humedades=0;

//Para calcular cuándo salió y cuándo se ocultó el sol
$salida_sol=0;
$puesta_sol=0;

$datos_mostrar=array(); //Array para las 24 horas del día, para cada hora mantiene la media de temperatura, humedad, viento, luz y precipitaciones

//Inicializar valores a 0
for($i =0; $i<24;$i++){
    $datos_mostrar[$i]["tm"]=0;
    $datos_mostrar[$i]["t"]=0;
    $datos_mostrar[$i]["hm"]=0;
    $datos_mostrar[$i]["vv"]=0;
    $datos_mostrar[$i]["luz"]=0;
    $datos_mostrar[$i]["pp"]=0;
}

//Coger cada tramo del día y calcular los datos medios.
foreach($json["datos"] as $tramo){
    $hora=date("G", $tramo['timestamp']); //Determinar a qué hora se recibieron estos datos
    $datos_mostrar[$hora]["tm"]+=$tramo['temperatura'];
    $datos_mostrar[$hora]["t"]+=1;
    $datos_mostrar[$hora]["hm"]+=$tramo['humedad'];
    $datos_mostrar[$hora]["vv"]+=$tramo['vel_viento'];
    $datos_mostrar[$hora]["luz"]+=$tramo['luz'];
    $datos_mostrar[$hora]["pp"]+=$tramo['precipitaciones'];

    $media_t+=$tramo['temperatura'];
    $temps++;
    $media_h+=$tramo['humedad'];
    $humedades++;

    if($tramo['luz'] > 300 && $salida_sol==0){
        $salida_sol=$tramo['timestamp'];
    }

    if($tramo['luz'] < 300 && $puesta_sol==0){
        $puesta_sol = $tramo['timestamp'];
    }
    if($tramo['luz'] > 300 && $puesta_sol!=0){
        $puesta_sol = 0;
    }
}


$puesta_sol_date=$puesta_sol!=0? date("H:m", $puesta_sol):"No disponible";
$salida_sol_date=$salida_sol!=0? date("H:m", $salida_sol):"No disponible";


//Strings de datos que se pasarán como parámetros al script de gráficos
$data_graph_t="";
$data_graph_h="";
$data_graph_vv="";
$data_graph_luz="";
$data_graph_pp="";

$horas="";
for($i = 0; $i < 24; $i++){
    if($i < 10)
        $horas .= "0".$i.":00,";
    else
        $horas .= $i.":00,";
}

for($i=0;$i<23;$i++){
    if($datos_mostrar[$i]["t"]==0) {
        $data_graph_t .= "0,";
    }else {
        $data_graph_t .= ($datos_mostrar[$i]["tm"] / $datos_mostrar[$i]["t"]) .",";
    }

    if($datos_mostrar[$i]["t"]==0) {
        $data_graph_h .= "0,";
    }else {
        $data_graph_h .= ($datos_mostrar[$i]["hm"] / $datos_mostrar[$i]["t"]) .",";
    }

    if($datos_mostrar[$i]["t"]==0) {
        $data_graph_vv .= "0,";
    }else {
        $data_graph_vv .= ($datos_mostrar[$i]["vv"] / $datos_mostrar[$i]["t"]) .",";
    }

    if($datos_mostrar[$i]["t"]==0) {
        $data_graph_luz .= "0,";
    }else {
        $data_graph_luz .= ($datos_mostrar[$i]["luz"] / $datos_mostrar[$i]["t"]) .",";
    }

    if($datos_mostrar[$i]["t"]==0) {
        $data_graph_pp .= "0,";
    }else {
        $data_graph_pp .= ($datos_mostrar[$i]["pp"] / $datos_mostrar[$i]["t"]) .",";
    }


}

$temperatura_media=$media_t/$temps;
$humedad_media=$media_h/$humedades;




?>

<head>
    <link href="css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="js/jquery-3.6.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Meteo-Arduino - Datos para <?php echo $date?></h5>
            </button>
        </div>
        <div class="modal-body">
            <a class="btn btn-primary" href=".">Volver al inicio</a>
            <br/><br/>
            <h2>Datos resumen del día</h2><hr>
            <p><?php
                echo "<b>- Temperatura media:</b> ".number_format($temperatura_media,1)."ºC</br>";
                echo "<b>- Humedad media:</b> ".number_format($humedad_media, 0)."%</br>";
                echo "<b>- Salida del sol: </b>".$salida_sol_date."</br>";
                echo "<b>- Puesta del sol: </b>".$puesta_sol_date."</br>";

                ?>
            </p>
            <br/>
            <h2>Tablas de datos</h2> <hr>
            <p style="text-align: center">
                <?php
                echo "<img src=\"graph.php?data=".$data_graph_t."&labels=".$horas."&title=Temperatura\">";
                echo "<img src=\"graph.php?data=".$data_graph_h."&labels=".$horas."&title=Humedad\">";
                echo "<img src=\"graph.php?data=".$data_graph_vv."&labels=".$horas."&title=Viento(Km/h)\">";
                echo "<img src=\"graph.php?data=".$data_graph_luz."&labels=".$horas."&title=Luz\">";
                echo "<img src=\"graph.php?data=".$data_graph_pp."&labels=".$horas."&title=Precipitaciones(l/m3)\">";
                ?></p>
        </div>
    </div>
</div>