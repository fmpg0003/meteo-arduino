<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
function json($message, $error_code){
    $arr=array('message' => $message, 'error_code'=>$error_code);
    echo json_encode($arr);
    return;
}

function err_auth(){
    json("Error de autenficación", 4);
}

if(empty($_POST)){
    json("No hay petición.", 1);
    die;
}

if(
    !isset($_POST['t']) ||
    !isset($_POST['h']) ||
    !isset($_POST['dv']) ||
    !isset($_POST['vv']) ||
    !isset($_POST['lu']) ||
    !isset($_POST['mm']) ||
    !isset($_POST['user']) ||
    !isset($_POST['pwd'])
){
    json("La petición es incompleta.", 2);
    die;
}

$mysqli=new mysqli("localhost", "meteo", "HayQueAprobarAmbientales", "meteo");
if($mysqli->connect_error){
    json("Error de conexión a la base de datos. Es posible que esté offline por mantenimiento.", 3);
}
$stmt=$mysqli->prepare("SELECT * FROM auth WHERE user = ?");
$stmt->bind_param("s", $_POST['user']);
$stmt->execute();
$res=$stmt->get_result();
if($res->num_rows == 0){
    err_auth();
    die;
}
$auth=$res->fetch_array();

if(!password_verify($_POST['pwd'], $auth['pwd'])){
    err_auth();
    die;
}

$now = time();
$stmt=$mysqli->prepare("INSERT INTO data VALUES (?,?,?,?,?,?,?)");
$stmt->bind_param("idisdid", $now, $_POST['t'], $_POST['h'], $_POST['dv'], $_POST['vv'], $_POST['lu'], $_POST['mm']);
$stmt->execute();
if($stmt->error){
    json("Error enviando la consulta a la base de datos. Algún parámetro puede ser incorrecto.", 5);
    die;
}

json("Datos almacenados correctamente.", 0);