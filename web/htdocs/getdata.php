<?php
require("includes/functions.php");
header('Content-Type: text/json');
function json($message, $error_code){
    $arr=array('message' => $message, 'error_code'=>$error_code);
    echo json_encode($arr, JSON_PRETTY_PRINT);
    return;
}

if(empty($_POST)){
    json("No hay petición.", 1);
    die;
}

if(
    !isset($_POST['fecha']) &&
    !isset($_POST['timestamp'])
){
    json("La petición es incompleta.", 2);
    die;
}
$timestamp = 0;
if(isset($_POST['fecha'])){
    $timestamp=strtotime($_POST['fecha']);
} else if(isset($_POST['timestamp'])){
    $timestamp=intval($_POST['timestamp']);
}

if(!$timestamp){
    json("La fecha solicitada es incorrecta.", 5);
    die;
}
$res=getDataFromDay($timestamp);

if($res === -1){
    json("Error de conexión a la base de datos. Es posible que esté offline por mantenimiento.", 3);
    return;
}

if($res === 0){
    json("No hay datos para este periodo de tiempo", 6);
    die;
}

echo $res;
die;